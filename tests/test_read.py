from riegl_tools import rieglrdb, rieglrxp


def test_read_rxp_file():
    header, data = rieglrxp.readFile("tests/131008_121124.mon.rxp")


def test_read_rdbx_file():
    header, data = rieglrdb.readFile("tests/230610_144337.residual.rdbx")
