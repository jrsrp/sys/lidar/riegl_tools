#!/bin/sh

# credit goes to umockdev author Martin Pitt martinpitt https://github.com/martinpitt/umockdev
ROOT=$(dirname $(realpath "$0"))


if [ -e "${MESON_SOURCE_ROOT:-}/.git" ] && VER=$(git -C "$MESON_SOURCE_ROOT" describe); then
    # make version number distribution friendly
    # this bit from Leo's script, but not sure of original source
    describe=$(git -C "$MESON_SOURCE_ROOT"  describe --tags --long)

    # extract the tag and commit count
    tag=$(echo $describe | sed 's/-.*//')
    count=$(echo $describe | sed 's/^[^-]*-//' | sed 's/-.*//')
    commit=$(echo $describe | sed 's/.*g//')

    # check if tag is present
    if [ "$count" -eq 0 ]; then
        # remove the "v" prefix from the tag
        VER=${tag#v}
    else
    # construct the new tag
        VER="$tag.dev$count+dirty"
    fi

    # when invoked as dist script, write the stamp; this is false when invoked from project.version()
    # if MESON_DIST_ROOT is defined then write to version file in there
    [ -z "${MESON_DIST_ROOT:-}" ] || echo "$VER" > "${MESON_DIST_ROOT}/.version"
    echo "$VER"
# when invoked from a tarball, it should be in the source root
elif [ -e "${ROOT}/.version" ]; then
    cat "${ROOT}/.version"
else
    echo "ERROR: Neither a git checkout nor .version, cannot determine version" >&2
    exit 1
fi
