from importlib.metadata import PackageNotFoundError, version

try:
    dist_name = __name__
    __version__ = version(dist_name)
except PackageNotFoundError:
    # If the package is not installed, don't add __version__
    pass
finally:
    del version, PackageNotFoundError
