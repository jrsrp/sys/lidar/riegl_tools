# Riegl Tools

This package contains Python extensions for reading files in the Riegl RDBX and RXP
formats into numpy arrays for processing.

Building this package requires access to one or both of the proprietary
[RDBLib](http://www.riegl.com/products/software-packages/rdblib/)
and [RiVLIB](http://www.riegl.com/index.php?id=224) libraries.
You will need a Riegl account to access these libraries.


## Building

This package assumes you have a C and a C++ compiler.


Before building this extension you will need Python and numpy installed. You will also need a C++
compiler (Note: on Windows this needs to MSVC to be compatible with Python, see the [Python Windows Compilers page](https://wiki.python.org/moin/WindowsCompilers)
for more information).

You will need to have the  [RDBLib](http://www.riegl.com/products/software-packages/rdblib/)
and/or [RiVLIB](http://www.riegl.com/index.php?id=224) unzipped somewhere on your machine. Set the
`RDBLIB_ROOT` environment variable to the location of the root of the RDBLib directory tree and the
`RIVLIB_ROOT` environment variable to the location of the root of thethe RiVLib directory tree.

The package should then be able to be installed:

```
RDBLIB_VERSION=2.4.2
RIVLIB_VERSION=2_7_0
export RIVLIB_ROOT=/opt/riegl/rivlib/rivlib-${RIVLIB_VERSION}-x86_64-linux-gcc5
export RDBLIB_ROOT=/opt/riegl/rdblib/rdblib-${RDBLIB_VERSION}-x86_64-linux

uv pip install .

```

If neither libraries are found an error will be raised.

## Usage

```
from riegl_tools import rieglrdb
from riegl_tools import rieglrxp

rdbheader, rdbdata = rieglrdb.readFile('190620_092323.rdbx')

rxpheader, rxpdata = rieglrxp.readFile('161122_092408.rxp')

```

See the docstrings of the above modules for more information
